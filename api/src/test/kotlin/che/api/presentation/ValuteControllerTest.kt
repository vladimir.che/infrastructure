package che.api.presentation

import assertk.assertThat
import assertk.assertions.isEqualTo
import che.api.model.toRaw
import che.api.repository.ValuteRepository
import che.api.model.Valute
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test

internal class ValuteControllerTest {

    private val valuteRepositoryMock = mockk<ValuteRepository>()
    private val sut = ValuteController(valuteRepositoryMock)

    @Test
    fun getValute() {
        val valutes = getValutes()
        every { valuteRepositoryMock.readValutes() } answers { valutes }

        val result = sut.valutes()

        assertThat(result).isEqualTo(valutes.values.map { it.toRaw() })
    }

    private fun getValutes() = mapOf(
        "TST" to Valute(
            id = "Test",
            numCode = 4,
            charCode = "test",
            nominal = 1,
            name = "test valute",
            currentValue = 0.5,
            previousValue = 0.6
        )
    )
}
