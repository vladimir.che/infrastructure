package che.api.model

import com.fasterxml.jackson.annotation.JsonProperty

data class Valute(
    @JsonProperty("ID")
    val id: String,
    @JsonProperty("NumCode")
    val numCode: Int,
    @JsonProperty("CharCode")
    val charCode: String,
    @JsonProperty("Nominal")
    val nominal: Int,
    @JsonProperty("Name")
    val name: String,
    @JsonProperty("Value")
    val currentValue: Double,
    @JsonProperty("Previous")
    val previousValue: Double
)
