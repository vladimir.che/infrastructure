package che.api.model

data class RawValute(
    val id: String,
    val numCode: Int,
    val charCode: String,
    val nominal: Int,
    val name: String,
    val currentValue: Double,
    val previousValue: Double
)

fun Valute.toRaw() = RawValute(
    this.id,
    this.numCode,
    this.charCode,
    this.nominal,
    this.name,
    this.currentValue,
    this.previousValue
)
