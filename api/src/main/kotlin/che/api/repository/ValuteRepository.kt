package che.api.repository

import che.api.model.Dates
import che.api.model.Valute

interface ValuteRepository {
    fun readDates():Dates
    fun readValutes():Map<String, Valute>
}
