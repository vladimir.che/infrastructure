package che.api.repository

import che.api.model.Dates
import che.api.model.Valute
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.stereotype.Repository

@Repository
class ValuteRepositoryImpl(val redisTemplate: RedisTemplate<String, String>, private val mapper: ObjectMapper) :
    ValuteRepository {
    private companion object {
        const val VALUTES = "valutes"
        const val DATE = "date"
        const val PREVIOUS_DATE = "previous_date"
    }

    override fun readDates(): Dates {
        val ops = redisTemplate.opsForValue()
        val date = ops.get(DATE) ?: ""
        val previousDate = ops.get(PREVIOUS_DATE) ?: ""
        return Dates(date, previousDate)
    }

    override fun readValutes(): Map<String, Valute> =
        redisTemplate.opsForHash<String, String>().entries(VALUTES).mapValues { mapper.readValue<Valute>(it.value) }
            .toMap()
}
