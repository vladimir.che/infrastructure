package che.api.presentation

import che.api.model.RawValute
import che.api.model.toRaw
import che.api.repository.ValuteRepository
import mu.KotlinLogging
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ValuteController(private val valuteRepository: ValuteRepository) {
    private val logger = KotlinLogging.logger { }

    @GetMapping("/valute/list")
    fun valutes(): List<RawValute> {
        val valutes = valuteRepository.readValutes()
        return valutes.values.map { it.toRaw() }
    }

    @GetMapping("/valute/dates")
    fun dates() = valuteRepository.readDates()
}
