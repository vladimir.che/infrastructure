provider "helm" {
    kubernetes {
        config_path = var.kube_config_path
    }
}

# Развертывание redis для приложения
resource "helm_release" "redis" {

    name = "redis"
    repository = "./helm-charts" #"https://prometheus-community.github.io/helm-charts"
    chart      = "redis"
    version = "16.8.9"
    namespace  = var.namespace
    create_namespace = true
set {
    name  = "auth.enabled"
    value = "false"
}
    set {
        name  = "architecture"
        value = "standalone"
    }
    set {
        name  = "master.persistence.enabled"
        value = "false"
    }
    set {
        name  = "metrics.enabled"
        value = "true"
    }
}
