## Terraform проект для разворачивания компонентов мониторинга и логирования в кластере kubernetes

- _prometheus + grafana_ - для сбора метрик
- _fluentd + elasticsearch + kibana_ - для сбора логов

### Требования к окружению

- Установленный terraform (>= 1.1.9)
- Созданный кластер kubernetes, где будут развернуты компоненты
- Установленная утилита для общения с kubernetes - kubectl c настроенным на кластер контекстом
- Установленный helm (>= 3.8.2)

### Что делает проект

- Создает namespace в kubernetes
- Разворачивает в созданном namespace указанные выше компоненты с необходимыми конфигурациями
  Helm чарты находятся в директории helm-charts (возникают проблемы со скачиванием)

### Запуск проекта

Выполняем инициализацию terraform в текущей директории

> terraform init

Запускаем развертывание компонентов. Настройки переменных указаны в файле terraform.tfvars.
Пример файла - terraform.tfvars.example

> terraform apply

На предложение о развертывании необходимо ответить "yes" (либо добавить флаг к команде _-auto-approve_ ).

### Проверка корректности развертывания

Проверка Grafana и Prometheus

> kubectl -n monitoring-logging port-forward svc/prometheus-grafana 80

Перейти на http://localhost и войти в графану (login/pass: admin/prom-operator).
Проверить, что метрики отображаются на дашбордах

Проверка elasticsearch

> kubectl -n monitoring-logging port-forward svc/elasticsearch-coordinating-only 80:9200

Перейти на http://localhost - на странице должен быть JSON с информацией о elasticsearch

Проверка fluentd и kibana

> kubectl -n monitoring-logging port-forward svc/kibana 80:5601

Перейти на http://localhost и войти в кибану.
Настроить паттерн индекс (префикс logstash) и проверить, что логи есть (значит fluentd также корректно настроен)

### Сворачивание проекта

Необходимо выполнить команду

> terraform destroy

На предложение о сворачивании необходимо ответить "yes" (либо добавить флаг к команде _-auto-approve_ ).
