provider "helm" {
  kubernetes {
    config_path = var.kube_config_path
  }
}

provider "kubernetes" {
  config_path = var.kube_config_path
}

# Создание неймспейса для компонентов мониторинга и логирования
resource "kubernetes_namespace" "monitoring-logging" {
  metadata {
    name = var.namespace
  }
}

# Развертывание prometheus grafana
# Пароли по умолчанию для Grafana  admin/prom-operator
# Для доступа к графане можно использовать проброс портов kubectl -n monitoring-logging port-forward svc/prometheus-grafana 80
resource "helm_release" "kube-prometheus-stack" {

  name = "prometheus"
  repository = "./helm-charts" #"https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"
  version = "35.0.3"
  namespace  = kubernetes_namespace.monitoring-logging.metadata[0].name

}

# Развертывание elasticsearch
#  kubectl -n monitoring-logging port-forward svc/elasticsearch-coordinating-only 80:9200
resource "helm_release" "elasticsearch" {

  name = "elasticsearch"
  repository =  "./helm-charts" #"https://charts.bitnami.com/bitnami"
  chart      = "elasticsearch"
  version    = "17.9.28"
  namespace  = kubernetes_namespace.monitoring-logging.metadata[0].name
  set {
    name  = "master.persistence.enabled"
    value = "false"
  }
  set {
    name  = "data.persistence.enabled"
    value = "false"
  }
}

# Развертывание kibana
# kubectl -n monitoring-logging port-forward svc/kibana 80:5601
resource "helm_release" "kibana" {

  name = "kibana"
  repository = "./helm-charts" #"https://charts.bitnami.com/bitnami"
  chart      = "kibana"
  version    = "9.3.17"
  namespace  = kubernetes_namespace.monitoring-logging.metadata[0].name
  set {
    name  = "elasticsearch.hosts[0]"
    value = "elasticsearch-coordinating-only"
  }
  set {
    name  = "elasticsearch.port"
    value = "9200"
  }
}

# Развертывание fluentd
resource "null_resource" "fluentd-config" {
  # Установка configmap для fluentd с настройками пересылки логов
  provisioner "local-exec" {
    command = "kubectl apply -f ./config/fluentd-config.yaml -n ${kubernetes_namespace.monitoring-logging.metadata[0].name}"
  }
}
resource "helm_release" "fluentd" {
  depends_on = [null_resource.fluentd-config, helm_release.elasticsearch]

  name = "fluentd"
  repository = "./helm-charts" #"https://charts.bitnami.com/bitnami"
  chart      = "fluentd"
  version    = "4.2.3"
  namespace  = kubernetes_namespace.monitoring-logging.metadata[0].name

  set {
    name  = "aggregator.configMap"
    value = "elasticsearch-output"
  }
}





