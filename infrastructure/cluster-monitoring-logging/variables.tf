variable "kube_config_path" {
  type = string
  description = "Путь к файлу конфигурации kubernetes"
}

variable "namespace" {
  type = string
  default = "monitoring-logging"
}

