variable "kube_config_path" {
  type = string
  description = "Путь к файлу конфигурации kubernetes"
  default = "~/.kube/config"
}

variable "gitlab_agent_token" {
  type = string
  description = "Токен регистрации агента gitlab для kubernetes"
}

variable "gitlab_url" {
  type = string
  description = "Адрес gitlab для регистрации runner"
  default = "https://gitlab.com/"
}

variable "gitlab_runner_token" {
  type = string
  description = "Токен регистрации runner gitlab"
}

