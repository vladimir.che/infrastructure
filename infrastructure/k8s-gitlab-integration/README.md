## Terraform проект для разворачивания компонентов gitlab в кластере kubernetes и их регистрации в gitlab

- gitlab-agent - для возможности развертывания приложений в кластере из gitlab
- gitlab-runner - для запуска пайплайнов gitlab

### Требования к окружению

- Установленный terraform (>= 1.1.9)
- Созданный кластер kubernetes, где будут развернуты компоненты
- Установленная утилита для общения с kubernetes - kubectl c настроенным на кластер контекстом
- Установленный helm (>= 3.8.2)
- Созданный проект в GitLab (gitlab.com)
- Данные для регистрации агента - можно получить при регистрации агента в gitlab:

> Infrastructure -> Kubernetes cluster -> Agent -> Connect the cluster (Agent) -> create

Нужные поля - config.token и config.kasAddress

- Данные для регистрации раннера

> Settings -> CI/CD -> Runners -> Specific runners

Нужен Url для регистрации и registration token.
Также стоит отключить "Enable shared runners for this project" в разделе Shared runners.

### Что делает проект

- Создает namespace в kubernetes
- Разворачивает в созданном namespace gitlab-agent и gitlab-runner.
  Helm charts для компонентов лежат в соответствующих поддиректориях gitlab-agent и gitlab-runner
  (возникли сложности с репозиторием https://charts.gitlab.io)
- Регистрирует компоненты в gitlab

### Запуск проекта

Выполняем инициализацию terraform в текущей директории

> terraform init

Запускаем развертывание компонентов. Настройки переменных указаны в файле terraform.tfvars.
Пример файла - terraform.tfvars.example

> terraform apply

На предложение о развертывании необходимо ответить "yes" (либо добавить флаг к команде _-auto-approve_ ).

### Проверка корректности развертывания

Проверить наличие развернутых подов

> kubectl get pod -n <созданный namespace>

Команда должна вернуть два развернутых пода.

Для проверки регистрации проверить статус раннера в gitlab (должен быть зеленый)

> Settings -> CI/CD -> Runners -> Specific runners

Проверить регистрацию агента (Connection status должен быть connected)

> Infrastructure -> Kubernetes cluster -> Agent

Для корректного выполнения пайплайна в GitLab в разделе *CI/CD - Variables* должна быть определена переменная DOCKER_PASS с паролем для dockerhub

### Сворачивание проекта

Необходимо выполнить команду

> terraform destroy

На предложение о сворачивании необходимо ответить "yes" (либо добавить флаг к команде _-auto-approve_ ).
