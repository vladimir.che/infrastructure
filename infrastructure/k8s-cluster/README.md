## Terraform проект для разворачивания кластера kubernetes в облаке Yandex Cloud

### Требования к окружению

- Установленный и настроенный на облако Yandex Cloud CLI (>=0.86.0)
- Установленный terraform (>= 1.1.9) с установленной конфигурацией провайдера yandex для terraform.
  Конфигурация находится в проекте k8s_cluster_install/config/terraform.rc
  Файл terraform.rc должен быть расположен в папке %APPDATA% для системы Windows.
  Для Linux есть отличия, см. [документацию](https://www.terraform.io/cli/config/config-file)
- Установленная утилита для общения с kubernetes - kubectl
- Настроенный bucket в Object Storage Yandex Cloud для удаленного хранения состояния terraform

### Что делает проект

- Создает сеть и подсеть для кластера k8s
- Создает service account для YC для развертывания, присваивает необходимые роли
- Создает Managed cluster for Kubernetes, Worker nodes для кластера
- Прописывает необходимые настройки для доступа к кластеру в конфиг файл kubernetes (по умолчанию ~/.kube/config)
  и устанавливает контекст по умолчанию на созданный кластер

### Запуск проекта

Выполняем инициализацию terraform в текущей директории, указав файл конфигурации backend для удаленного сохранения состояния terraform.
Пример файла конфигурации s3.tfbackend.example

> terraform init --backend-config=s3.tfbackend

Запускаем развертывание кластера. Настройки переменных указаны в файле terraform.tfvars.
Пример файла - terraform.tfvars.example

> terraform apply

На предложение о развертывании необходимо ответить "yes" (либо добавить флаг к команде _-auto-approve_ ).
По окончании конфигурации кластера (около 10 минут) в выводе будет указана
переменная cluster_external_v4_endpoint с адресом API сервера кластера.

### Проверка корректности развертывания

Можно выполнить любую команду утилитой kubectl (настраивается автоматически в процессе разворачивания кластера), например

> kubectl get ns

Команда должна вернуть результат без ошибок

### Сворачивание проекта

Необходимо выполнить команду

> terraform destroy

На предложение о сворачивании необходимо ответить "yes" (либо добавить флаг к команде _-auto-approve_ ).
