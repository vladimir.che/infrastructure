/*
 * The settings file is used to specify which builds to include in your build.
 *
 * Detailed information about configuring a composite builds in Gradle can be found
 * in the user manual at https://docs.gradle.org/current/userguide/composite_builds.html
 */
rootProject.name = "che"

includeBuild("front")
includeBuild("api")
includeBuild("integration")

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
    }
}
