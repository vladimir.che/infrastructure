package che.front.config

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient

@Configuration
@ConfigurationProperties(prefix = "api")
class AppConfig {
    lateinit var host:String
    var port:Int = 8080

    @Bean
    fun webClient() = WebClient.builder().baseUrl("http://${host}:${port}").build()
    @Bean
    fun mapper() = jacksonObjectMapper()
}
