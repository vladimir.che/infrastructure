package che.front.controller

import che.front.model.Dates
import che.front.model.Valute
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.bodyToMono

@Controller
class ValuteController(private val client:WebClient, private val mapper: ObjectMapper) {

    private val valutes = "/valute/list"
    private val dates = "/valute/dates"

    @GetMapping("")
    suspend fun valute(model:Model): String {

        model.addAttribute("valutes",valutes())
        model.addAttribute("dates",dates())
        return "index"
    }

    private suspend fun valutes(): List<Valute> {
        val response = client.get().uri(valutes).retrieve().awaitBody<String>()
        return mapper.readValue<List<Valute>>(response).sortedBy { it.charCode }
    }
    private suspend fun dates(): Dates {
        val response = client.get().uri(dates).retrieve().awaitBody<String>()
        return mapper.readValue(response)
    }
}
