package che.front.model


data class Valute(
    val id: String,
    val numCode: Int,
    val charCode: String,
    val nominal: Int,
    val name: String,
    val currentValue: Double,
    val previousValue: Double
)
