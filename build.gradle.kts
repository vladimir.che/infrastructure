listOf("clean", "assemble", "testClasses", "check", "test", "build").forEach { taskName ->
    tasks.register(taskName) {
        dependsOn(gradle.includedBuilds.map { it.task(":$taskName") })
    }
}
