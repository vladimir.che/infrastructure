package che.integration.service

import che.integration.model.Course
import che.integration.model.Valute
import che.integration.repository.CourseRepository
import che.integration.repository.ValuteRepository
import io.mockk.*
import org.junit.jupiter.api.Test
import java.sql.Timestamp
import java.time.LocalDateTime

internal class CourseServiceImplTest {

    private val valuteRepositoryMock = mockk<ValuteRepository>()
    private val courseRepositoryMock = mockk<CourseRepository>()

    private val sut = CourseServiceImpl(valuteRepositoryMock, courseRepositoryMock)

    @Test
    fun updateCourse() {
        val course = getCourse()
        val valute = course.valute
        every { courseRepositoryMock.getCurrentCourse() } answers { course }
        every { valuteRepositoryMock.saveDates(course.date, course.previousDate) } just Runs
        every { valuteRepositoryMock.saveValutes(valute) } just Runs

        sut.updateCourse()

        verifySequence {
            courseRepositoryMock.getCurrentCourse()
            valuteRepositoryMock.saveDates(course.date, course.previousDate)
            valuteRepositoryMock.saveValutes(valute)
        }
    }

    private fun getCourse() = Course(
        date = Timestamp.valueOf(LocalDateTime.now()),
        previousDate = Timestamp.valueOf(LocalDateTime.now()),
        valute = mapOf(
            "TST" to Valute(
                id = "Test",
                numCode = 4,
                charCode = "test",
                nominal = 1,
                name = "test valute",
                currentValue = 0.5,
                previousValue = 0.6
            )
        )
    )
}
