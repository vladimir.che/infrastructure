package che.integration.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient

@Configuration
class WebClientConfig {

    @Bean
    fun webClient() = WebClient.builder().baseUrl("https://www.cbr-xml-daily.ru").build()
}
