package che.integration.repository

import che.integration.model.Valute
import java.sql.Timestamp

interface ValuteRepository {
    fun saveDates(date:Timestamp, previousDate:Timestamp)
    fun saveValutes(valutes:Map<String,Valute>)
}
