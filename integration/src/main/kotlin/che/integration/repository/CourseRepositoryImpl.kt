package che.integration.repository

import che.integration.model.Course
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.stereotype.Repository
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono

@Repository
class CourseRepositoryImpl(private val webClient: WebClient, private val mapper: ObjectMapper) : CourseRepository {

    override fun getCurrentCourse(): Course {
        val response = webClient.get().uri("/daily_json.js").retrieve().bodyToMono<String>().block()
            ?: throw RuntimeException("Failed load course")
        return mapper.readValue(response)
    }
}
