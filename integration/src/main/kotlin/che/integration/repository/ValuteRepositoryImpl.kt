package che.integration.repository

import che.integration.model.Valute
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.stereotype.Repository
import java.sql.Timestamp

@Repository
class ValuteRepositoryImpl(val redisTemplate: RedisTemplate<String, String>, private val mapper: ObjectMapper) : ValuteRepository {
    private companion object {
        const val VALUTES = "valutes"
        const val DATE = "date"
        const val PREVIOUS_DATE = "previous_date"
    }

    override fun saveDates(date: Timestamp, previousDate: Timestamp) {
        val ops = redisTemplate.opsForValue()
        ops.set(DATE,date.toLocalDateTime().toLocalDate().toString())
        ops.set(PREVIOUS_DATE,previousDate.toLocalDateTime().toLocalDate().toString())

    }

    override fun saveValutes(valutes: Map<String, Valute>) =
        redisTemplate.opsForHash<String, String>().putAll(VALUTES, valutes.mapValues { mapper.writeValueAsString(it.value) })

}
