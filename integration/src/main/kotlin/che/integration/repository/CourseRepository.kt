package che.integration.repository

import che.integration.model.Course

interface CourseRepository {
    fun getCurrentCourse():Course
}
