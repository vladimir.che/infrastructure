package che.integration.scheduling

import che.integration.service.CourseService
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class CourseScheduler(private val courseService: CourseService) {

    @Scheduled(fixedDelay = 3_600_000, initialDelay = 2_000)
    fun updateCourse() = courseService.updateCourse()
}
