package che.integration.service

import che.integration.repository.CourseRepository
import che.integration.repository.ValuteRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service

@Service
class CourseServiceImpl(
    private val valuteRepository: ValuteRepository,
    private val courseRepository: CourseRepository
) : CourseService {
    private val logger = KotlinLogging.logger {  }
    override fun updateCourse() {
        val course = courseRepository.getCurrentCourse()
        valuteRepository.saveDates(course.date, course.previousDate)
        valuteRepository.saveValutes(course.valute)
        logger.info { "Saved ${course.valute.size} valutes" }
    }
}
