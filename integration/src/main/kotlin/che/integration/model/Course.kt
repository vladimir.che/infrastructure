package che.integration.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.sql.Timestamp

@JsonIgnoreProperties("PreviousURL", "Timestamp")
data class Course(
    @JsonProperty("Date")
    val date: Timestamp,
    @JsonProperty("PreviousDate")
    val previousDate: Timestamp,
    @JsonProperty("Valute")
    val valute: Map<String, Valute>
)
